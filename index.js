// Khai báo thư viên Express
const express = require("express");

// Khởi tạo app express
const app = express();

// Khai báo cổng chạy app 
const port = 8000;

// khai báo mongoose 
var mongoose = require('mongoose');

// Kết nối với MongoDB:
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course", function(error) {
    if (error) throw error;
    console.log('Successfully connected');
})


// Khai báo router app
const courseRouter = require("./app/routes/courseRouter");
const reviewRouter = require("./app/routes/reviewRouter");
const reviewModel = require("./app/models/reviewModel");
const courseModel = require("./app/models/courseModel");

app.use((request, response, next) => {
    console.log("Current time: ", new Date());
    next();
})

app.use((request, response, next) => {
    console.log("Request method: ", request.method);
    next();
})

// Khai báo API /
app.get("/", (request, response) => {
    console.log("Call API GET /");

    response.json({
        message: "Devcamp Middleware Express APP"
    })
})

// App sử dụng router
app.use("/api", courseRouter);
app.use("/api", reviewRouter);

// Chạy app trên cổng
app.listen(port, () => {
    console.log("App listening on port:", port);
})