const mongoose = require("mongoose")

const Schema = mongoose.Schema

//Khởi tạo instance reviewSchema 
const reviewSchenma = new Schema({

    start: {
        type: Number,
        deault: 0
    },
    node: {
        type: String,
        require: false
    },
}, {
    //Lưu dấu bảng ghi được cập nhật vào thời gian nào
    timestamps: true
})

// Biên dịch một Book Model từ bookscheme
module.exports = mongoose.model("Review", reviewSchenma)