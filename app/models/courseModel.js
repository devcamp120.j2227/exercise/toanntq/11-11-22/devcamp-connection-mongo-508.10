// Khai báo thư viện mongo
const mongoose = require("mongoose")

//Khai báo class Schema
const Schema = mongoose.Schema

//Khởi tạo instance courseSchema 
const courseSchema = new Schema({
    title: {
        type: String,
        require: true,
        unique: true
    },
    description: {
        type: String,
        require: false
    },
    noStudent: {
        type: Number,
        default: 0
    },
    // Một course có nhiều review thêm dấu []
    review: [{
        type: mongoose.Types.ObjectId,
        ref: "Review"
    }]

    // Một course có 1 review thêm dấu []
    // review: {
    //     type: mongoose.Types.ObjectId,
    //     ref: "Review"
    // }


}, {
    //Lưu dấu bảng ghi được cập nhật vào thời gian nào
    timestamps: true
})

// Biên dịch một Book Model từ bookscheme
module.exports = mongoose.model("Course", courseSchema)